<?php namespace Qchsoft\ServiceForm;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents(){
        return [
            'QchSoft\ServiceForm\Components\ServiceList' => 'ServiceList',
            'QchSoft\ServiceForm\Components\ServiceForm' => 'ServiceForm',
            
        ];
    }

    public function registerSettings()
    {
    }

    public function registerListColumnTypes(){
        return [
            // A local method, i.e $this->evalUppercaseListColumn()
            'icon' => [$this, 'setIconStatus'],

        ];

        
    }

    public function setIconStatus($value, $column, $record){
        $class = "";
        $icon = "";
       
        switch ($value) {
            case '1':
                $class ="list-badge badge-warning";
                $icon = "<i class='icon-exclamation'></i>";
                break;
            
            case '2':
                $class ="list-badge badge-success";
                $icon = "<i class='icon-check'></i>";
                break;
            case '3':
                $class ="list-badge badge-danger";
                $icon = "<i class='icon-times'></i>";
                break;

            default:
                $class ="list-badge badge-info";
                $icon = "<i class='icon-info'></i>";
                break;
        }
        return "<span class='$class'> $icon </span>".$record["status"]["name"];
    }
}
