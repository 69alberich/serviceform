<?php namespace QchSoft\ServiceForm\Components;

use Cms\Classes\ComponentBase;
use QchSoft\ServiceForm\Models\Form;
use QchSoft\ServiceForm\Models\Presentation;
use Input;
use Mail;

class ServiceForm extends ComponentBase{

    public function componentDetails(){
        return [
            'name'        => 'allow service form',
            'description' => 'methods for service form',
        ];
    }


    public function getPhoneCodes(){
        $service_url = 'https://restcountries.eu/rest/v2/all';
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            //die('error occured during curl exec. Additioanl info: ' . var_export($info));
            return array('+58' => '+58' );
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
            //die('error occured: ' . $decoded->response->errormessage);
           return array('+58' => '+58' );
  
        }else{
          return $decoded;
        }
    }

    public function onSetPresentationIdPage(){
        $data = post();
        $this->page["presentationId"] = $data["id"];
    }

    public function onSetServiceCodePage(){
        $data = post();
        $this->page["serviceCode"] = $data["code"];
    }

    
    public function onSendForm(){
        $data = post();
        $data["status_id"] = 1;
        $presentation = Presentation::find($data["presentation_id"]);

        unset($data["presentation_id"]);

        $form = Form::create($data);
        
        $form->presentation()->add($presentation);

        $data["username"] = $form->name." ".$form->lastname;

        try {
            Mail::send("qchsoft.formmegadattaextension::email.status",
            $data, function($message) use ($form) {
            /*foreach ($emailList as $email) {
                $message->to($email);
            }*/
            //$message->cc("alberich2052@gmail.com");
            $message->to($form->email);
            });
            

        } catch (Exception $ex) {

            trace_log($ex);

        }

        //$emailList = explode(",", ShopaHolicSettings::get("creating_order_manager_email_list"));
        //$form = array("form" => $data);
       
        /*
        try {
            Mail::send("qchsoft.yatchextension::mail.contact-form",
            $form, function($message) use ($emailList) {
            foreach ($emailList as $email) {
                $message->to($email);
            }
            //$message->to("alberich2052@gmail.com");
            
            });
            return Response::json(['success' => true]);
            
        } catch (\Throwable $th) {

            return Response::json(['success' => false]);

        }
        */
        
    }
}



