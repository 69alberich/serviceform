<?php namespace QchSoft\ServiceForm\Components;

use Cms\Classes\ComponentBase;
use QchSoft\ServiceForm\Models\Service;
use QchSoft\ServiceForm\Models\Presentation;
use Input;

class ServiceList extends ComponentBase{

    public function componentDetails(){
        return [
            'name'        => 'Display Service List',
            'description' => 'methods for service model',
        ];
    }

    function getByCode($arCodes){
       return $arService = Service::code($arCodes)->first();
    }

    function getPresentation($id){
        
        return Presentation::where("id", $id)->first();
    }
}