<?php namespace Qchsoft\ServiceForm\Models;

use Qchsoft\ServiceForm\Models\Status;
use Qchsoft\Location\Models\Zone;
use Qchsoft\ServiceForm\Models\Presentation;

use Model;

/**
 * Model
 */
class Form extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_serviceform_form';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'status' => Status::class,
        'zone' => Zone::class
    ];

    public $belongsToMany = [
        'presentation' => [Presentation::class, 'table' => 'qchsoft_serviceform_form_service']
    ];

    public $fillable = [
        "email", "name", "lastname", "phone", "comment", "address",
        "status_id", "zone_id"
    ];
}
