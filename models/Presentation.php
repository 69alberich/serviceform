<?php namespace Qchsoft\ServiceForm\Models;

use Qchsoft\ServiceForm\Models\Service;
use Model;

/**
 * Model
 */
class Presentation extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_serviceform_presentation';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        "service" => Service::class
    ];
}
