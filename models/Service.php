<?php namespace Qchsoft\ServiceForm\Models;

use Qchsoft\ServiceForm\Models\Category;
use Qchsoft\ServiceForm\Models\Presentation;
use Model;

/**
 * Model
 */
class Service extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_serviceform_services';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        "category" => Category::class
    ];

    public $hasMany = [
        "presentation" => Presentation::class
    ];

    public function scopeCode($query, $arCodes){
        
        return $query->whereIn('code', $arCodes);
        //->orderByRaw("FIND_IN_SET(city_id,'$arCityIds')");
    }
}
