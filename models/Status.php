<?php namespace Qchsoft\ServiceForm\Models;

use Model;

/**
 * Model
 */
class Status extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_serviceform_form_status';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
