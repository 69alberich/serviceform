<?php namespace Qchsoft\ServiceForm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftServiceformCategory extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_serviceform_category', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('code');
            $table->text('preview_text');
            $table->text('description');
            $table->string('slug');
            $table->integer('active');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_serviceform_category');
    }
}
