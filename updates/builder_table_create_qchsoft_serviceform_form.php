<?php namespace Qchsoft\ServiceForm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftServiceformForm extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_serviceform_form', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('email');
            $table->string('name');
            $table->string('lastname');
            $table->string('phone');
            $table->text('comment');
            $table->text('address');
            $table->integer('status_id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('zone_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_serviceform_form');
    }
}
