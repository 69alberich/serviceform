<?php namespace Qchsoft\ServiceForm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftServiceformFormService extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_serviceform_form_service', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('presentation_id');
            $table->integer('form_id');
            $table->primary(['presentation_id', 'form_id'], 'form_presentation_key');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_serviceform_form_service');
    }
}
