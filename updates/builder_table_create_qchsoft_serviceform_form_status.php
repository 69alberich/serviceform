<?php namespace Qchsoft\ServiceForm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftServiceformFormStatus extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_serviceform_form_status', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('description');
            $table->string('code');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_serviceform_form_status');
    }
}
