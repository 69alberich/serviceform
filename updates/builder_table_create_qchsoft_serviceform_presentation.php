<?php namespace Qchsoft\ServiceForm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftServiceformPresentation extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_serviceform_presentation', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('preview_text');
            $table->text('description');
            $table->decimal('price', 8, 2);
            $table->integer('service_id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_serviceform_presentation');
    }
}
