<?php namespace Qchsoft\ServiceForm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftServiceformServices extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_serviceform_services', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('preview_text');
            $table->text('description');
            $table->integer('category_id');
            $table->string('slug');
            $table->string('code');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_serviceform_services');
    }
}
